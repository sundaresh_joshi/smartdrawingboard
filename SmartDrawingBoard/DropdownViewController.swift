//
//  DropdownViewController.swift
//  Demo
//
//  Created by Radhika on 8/23/16.
//  Copyright © 2016 yml. All rights reserved.
//

import UIKit

enum DropDown { case color, tool, background}

var backgroundImageSettings = (change:"Choose", remove:"Remove")

protocol DropdownViewControllerDelegate: class {                 // protocol declaration
    func didSelectItem(item: String, type: DropDown)
}


class DropdownViewController: UITableViewController{
    weak var delegate: DropdownViewControllerDelegate?
    var items = [String]()
    
    var type = DropDown.color

  
    // MARK: - Viewcylcle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "LabelCell")
        
        switch type {
        case .color:
            items = Color.colorsArr
        case .tool:
            items = Tool.toolsArr
        case .background:
            items = [backgroundImageSettings.change,backgroundImageSettings.remove]
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

   // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count
    }

     
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "LabelCell")
        cell.textLabel!.text = items [indexPath.row]
        return cell;
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        let cell = tableView.cellForRow(at: indexPath)
        if let del = delegate {
            del.didSelectItem(item: cell?.textLabel?.text ?? "", type: type)
            dismiss(animated: true, completion: nil)
        }
       }
     }
