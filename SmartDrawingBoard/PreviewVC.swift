//
//  PreviewVC.swift
//  SmartDrawingBoard
//
//  Created by Sundaresh Joshi on 09/10/17.
//  Copyright © 2017 Sundaresh Joshi. All rights reserved.
//

import UIKit

class PreviewVC: UIViewController {
    
    @IBOutlet weak var bgImage: UIImageView!
    var image : UIImage?

    override func viewDidLoad() {
        super.viewDidLoad()

        bgImage.image = image
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveToAlbumAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
