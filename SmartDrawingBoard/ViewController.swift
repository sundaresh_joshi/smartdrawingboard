//
//  ViewController.swift
//  SmartDrawingBoard
//
//  Created by Sundaresh Joshi on 05/10/17.
//  Copyright © 2017 Sundaresh Joshi. All rights reserved.
//

import UIKit

enum Color: String {
    case red   = "Red"
    case blue  = "Blue"
    case green = "Green"
    case black = "Black"
    
    static let colorsArr = [Color.black.rawValue,Color.red.rawValue, Color.blue.rawValue, Color.green.rawValue]
}

enum Tool: String {
    case eraser      = "Eraser"
    case pen         = "Pen"
    case line        = "Line"
    case arrow       = "Arrow"
    case rect        = "Rect (Stroke)"
    case rectFill    = "Rect (Fill)"
    case ellipse     = "Ellipse (Stroke)"
    case ellipseFill = "Ellipse (Fill)"
    case text        = "Text"
    
    static let toolsArr = [Tool.eraser.rawValue, Tool.pen.rawValue, /*Tool.line.rawValue, Tool.arrow.rawValue, Tool.rect.rawValue, Tool.rectFill.rawValue, Tool.ellipse.rawValue, Tool.ellipseFill.rawValue, */Tool.text.rawValue]
}

class ViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var drawingView: ACEDrawingView!
    @IBOutlet weak var undoBtn: UIBarButtonItem!
    @IBOutlet weak var redoBtn: UIBarButtonItem!
    @IBOutlet weak var clearBtn: UIBarButtonItem!
    @IBOutlet weak var saveBtn: UIBarButtonItem!
    @IBOutlet weak var colorsBtn: UIButton!
    @IBOutlet weak var toolsBtn: UIButton!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var previewBtn: UIBarButtonItem!    
    @IBOutlet weak var topToolBar: UIToolbar!
    @IBOutlet weak var bottomToolBar: UIToolbar!
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var drawingViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var drawingViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var drawingViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var drawingViewRightConstraint: NSLayoutConstraint!
    
    var imagePicked : UIImage?
    
    var viewSize = CGSize.zero
    
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        drawingView.delegate = self
        viewSize = view.bounds.size
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateButtonStatus() {
        undoBtn.isEnabled = drawingView.canUndo()
        redoBtn.isEnabled = drawingView.canRedo()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        coordinator.animate(alongsideTransition: { [weak self] (UIViewControllerTransitionCoordinatorContext) -> Void in
            
            guard let `self` = self else {return}
            self.updateDrawingPaths(size: size)
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            //refresh view once rotation is completed not in will transition as it returns incorrect frame size.Refresh here
            self.view.layoutIfNeeded()
            self.drawingView.updatePaths()
        })
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    func updateDrawingPaths(size: CGSize) {
        let orient = UIApplication.shared.statusBarOrientation
        
        let imageSize = self.imagePicked?.size ?? self.viewSize
        
        switch orient {
            
        case .portrait:
            
            let aspectRatio = imageSize.width / imageSize.height
            let requiredHeight = size.width / aspectRatio
            
            self.drawingViewRightConstraint.constant = 0
            self.drawingViewLeftConstraint.constant = 0
            self.drawingViewTopConstraint.constant = size.height/2 - requiredHeight / 2
            self.drawingViewBottomConstraint.constant = size.height/2 - requiredHeight / 2
            
        case .landscapeLeft,.landscapeRight :
            
            let aspectRatio = imageSize.height / imageSize.width
            let requiredWidth = size.height / aspectRatio
            
            self.drawingViewTopConstraint.constant = 0
            self.drawingViewBottomConstraint.constant = 0
            self.drawingViewLeftConstraint.constant = size.width / 2 - requiredWidth / 2
            self.drawingViewRightConstraint.constant = size.width / 2 - requiredWidth / 2
            
        default: break
        }
    }
    
    
    // MARK: - IBActions
    @IBAction func undoBtnAction(_ sender: Any) {
        drawingView.undoLatestStep()
        updateButtonStatus()
    }

    @IBAction func redoBtnAction(_ sender: Any) {
        drawingView.redoLatestStep()
        updateButtonStatus()
    }
    
    @IBAction func clearBtnAction(_ sender: Any) {
        drawingView.clear()
        bgImage.image = nil
        updateButtonStatus()
        imagePicked = nil
        
        let orient = UIApplication.shared.statusBarOrientation
        let size = orient == .portrait ? viewSize : CGSize(width: viewSize.height, height: viewSize.width)
        updateDrawingPaths(size: size)
        view.layoutIfNeeded()
    }
    
    @IBAction func changeBackgroundImageAction(_ sender: Any) {
        guard UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiom.pad else {
            let tableViewController = DropdownViewController()
            tableViewController.type = .background
            tableViewController.delegate = self
            tableViewController.modalPresentationStyle = UIModalPresentationStyle.popover
            tableViewController.preferredContentSize = CGSize(width: 200, height: 200)
            
            let popoverPresentationController = tableViewController.popoverPresentationController
            popoverPresentationController?.sourceRect = CGRect(x: 0, y: 0, width: (sender as AnyObject).frame.size.width, height: (sender as AnyObject).frame.size.height)
            popoverPresentationController?.sourceView = sender as! UIButton
            
            present(tableViewController, animated: true, completion: nil)
            
            return
        }
        
        let actionSheet = UIAlertController(title: "", message: "Background Image", preferredStyle: .actionSheet)
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let changeBgImageAction = UIAlertAction(title: backgroundImageSettings.change, style: .default) { action -> Void in
            self.openImagePicker()
        }
        let removeBgAction = UIAlertAction(title: backgroundImageSettings.remove, style: .default) { action -> Void in
            self.bgImage.image = nil
        }

        actionSheet.addAction(cancelActionButton)
        actionSheet.addAction(changeBgImageAction)
        actionSheet.addAction(removeBgAction)
        
        self.present(actionSheet, animated: true, completion: nil)

    }
    
    @IBAction func previewBtnAction(_ sender: Any) {
        guard let image = drawingView.image else { return }
        let previewVC = PreviewVC()
        let previewImage = bgImage.image != nil ? drawingView.applyDraw(to: bgImage.image) : image
        if bgImage.image != nil {
            previewImage?.resizableImage(withCapInsets: bgImage.image!.capInsets, resizingMode: bgImage.image!.resizingMode)
        }
        previewVC.image = previewImage
        present(previewVC, animated: true, completion: nil)
    }
    
    
    @IBAction func colorsBtnAction(_ sender: Any) {
        
        guard UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiom.pad else {
            let tableViewController = DropdownViewController()
            tableViewController.type = .color
            tableViewController.delegate = self
            tableViewController.modalPresentationStyle = UIModalPresentationStyle.popover
            tableViewController.preferredContentSize = CGSize(width: 200, height: 200)
            
            let popoverPresentationController = tableViewController.popoverPresentationController
            popoverPresentationController?.sourceRect = CGRect(x: 0, y: 0, width: (sender as AnyObject).frame.size.width, height: (sender as AnyObject).frame.size.height)
            popoverPresentationController?.sourceView = sender as! UIButton
            
            present(tableViewController, animated: true, completion: nil)
            
            return
        }
        
        let actionSheet = UIAlertController(title: "Color", message: "Choose marker color", preferredStyle: .actionSheet)
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let redActionButton = UIAlertAction(title: Color.red.rawValue, style: .default) { action -> Void in
            self.drawingView.lineColor = .red
            self.colorsBtn.setTitle(Color.red.rawValue, for: .normal)
        }
        let blueActionButton = UIAlertAction(title: Color.blue.rawValue, style: .default) { action -> Void in
            self.drawingView.lineColor = .blue
            self.colorsBtn.setTitle(Color.blue.rawValue, for: .normal)
        }
        let greenActionButton = UIAlertAction(title: Color.green.rawValue, style: .default) { action -> Void in
            self.drawingView.lineColor = .green
            self.colorsBtn.setTitle(Color.green.rawValue, for: .normal)
        }
        let blackActionButton = UIAlertAction(title: Color.black.rawValue, style: .default) { action -> Void in
            self.drawingView.lineColor = .black
            self.colorsBtn.setTitle(Color.black.rawValue, for: .normal)
        }
        actionSheet.addAction(cancelActionButton)
        actionSheet.addAction(blackActionButton)
        actionSheet.addAction(redActionButton)
        actionSheet.addAction(blueActionButton)
        actionSheet.addAction(greenActionButton)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func toolsBtnAction(_ sender: Any) {
        
        guard UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiom.pad else {
            let tableViewController = DropdownViewController()
            tableViewController.type = .tool
            tableViewController.delegate = self
            tableViewController.modalPresentationStyle = UIModalPresentationStyle.popover
            tableViewController.preferredContentSize = CGSize(width: 300, height: 300)
            
            let popoverPresentationController = tableViewController.popoverPresentationController
            popoverPresentationController?.sourceRect = CGRect(x: 0, y: 0, width: (sender as AnyObject).frame.size.width, height: (sender as AnyObject).frame.size.height)
            popoverPresentationController?.sourceView = sender as! UIButton
            
            present(tableViewController, animated: true, completion: nil)
            
            return
        }
        
        let actionSheet = UIAlertController(title: "Tools", message: "Choose a Tool", preferredStyle: .actionSheet)
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
        }
        let eraserActionButton = UIAlertAction(title: Tool.eraser.rawValue, style: .default) { action -> Void in
            self.drawingView.drawTool = ACEDrawingToolTypeEraser
            self.toolsBtn.setTitle(Tool.eraser.rawValue, for: .normal)
        }
        let penActionButton = UIAlertAction(title: Tool.pen.rawValue, style: .default) { action -> Void in
            self.drawingView.drawTool = ACEDrawingToolTypePen
            self.toolsBtn.setTitle(Tool.pen.rawValue, for: .normal)
        }
//        let lineActionButton = UIAlertAction(title: Tool.line.rawValue, style: .default) { action -> Void in
//            self.drawingView.drawTool = ACEDrawingToolTypeLine
//            self.toolsBtn.setTitle(Tool.line.rawValue, for: .normal)
//        }
//        let arrowActionButton = UIAlertAction(title: Tool.arrow.rawValue, style: .default) { action -> Void in
//            self.drawingView.drawTool = ACEDrawingToolTypeArrow
//            self.toolsBtn.setTitle(Tool.arrow.rawValue, for: .normal)
//        }
//        let rectActionButton = UIAlertAction(title: Tool.rect.rawValue, style: .default) { action -> Void in
//            self.drawingView.drawTool = ACEDrawingToolTypeRectagleStroke
//            self.toolsBtn.setTitle(Tool.rect.rawValue, for: .normal)
//        }
//        let rectFillActionButton = UIAlertAction(title: Tool.rectFill.rawValue, style: .default) { action -> Void in
//            self.drawingView.drawTool = ACEDrawingToolTypeRectagleFill
//            self.toolsBtn.setTitle(Tool.rectFill.rawValue, for: .normal)
//        }
//        let ellipseActionButton = UIAlertAction(title: Tool.ellipse.rawValue, style: .default) { action -> Void in
//            self.drawingView.drawTool = ACEDrawingToolTypeEllipseStroke
//            self.toolsBtn.setTitle(Tool.ellipse.rawValue, for: .normal)
//        }
//        let ellipseFillActionButton = UIAlertAction(title: Tool.ellipseFill.rawValue, style: .default) { action -> Void in
//            self.drawingView.drawTool = ACEDrawingToolTypeEllipseFill
//            self.toolsBtn.setTitle(Tool.ellipseFill.rawValue, for: .normal)
//        }
        let textActionButton = UIAlertAction(title: Tool.text.rawValue, style: .default) { action -> Void in
            self.drawingView.drawTool = ACEDrawingToolTypeDraggableText
            self.toolsBtn.setTitle(Tool.text.rawValue, for: .normal)
        }
        
        actionSheet.addAction(cancelActionButton)
        actionSheet.addAction(eraserActionButton)
        actionSheet.addAction(penActionButton)
//        actionSheet.addAction(lineActionButton)
//        actionSheet.addAction(arrowActionButton)
//        actionSheet.addAction(rectActionButton)
//        actionSheet.addAction(rectFillActionButton)
//        actionSheet.addAction(ellipseActionButton)
//        actionSheet.addAction(ellipseFillActionButton)
        actionSheet.addAction(textActionButton)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func openImagePicker() {
        DispatchQueue.main.async {
            let imagePicker = UIImagePickerController()
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .photoLibrary
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
}

extension ViewController: ACEDrawingViewDelegate {
    
    func drawingView(_ view: ACEDrawingView!, willBeginDrawUsing tool: ACEDrawingTool!) {
        topToolBar.isHidden = true
        bottomToolBar.isHidden = true
    }
    
    func drawingView(_ view: ACEDrawingView!, didEndDrawUsing tool: ACEDrawingTool!) {
        updateButtonStatus()
        topToolBar.isHidden = false
        bottomToolBar.isHidden = false
        textView.isHidden = true
        textView.text = ""
        textView.resignFirstResponder()
    }
    
    func drawingView(_ view: ACEDrawingView!, didStartEditingText tool: ACEDrawingTool!, _ text: String!) {
        textView.isHidden = false
        textView.text = text
        textView.becomeFirstResponder()
    }
}

extension ViewController: DropdownViewControllerDelegate {
    func didSelectItem(item: String, type: DropDown) {
        
        switch type {
        case .color:
            guard let color = Color.init(rawValue: item) else { return }
            colorsBtn.setTitle(item, for: .normal)
            switch color {
            case .red:
                drawingView.lineColor = .red
            case .blue:
                drawingView.lineColor = .blue
            case .green:
                drawingView.lineColor = .green
            case .black:
                drawingView.lineColor = .black
            }
        case .tool:
            guard let tool = Tool.init(rawValue: item) else { return }
            toolsBtn.setTitle(item, for: .normal)
            switch tool {
            case .pen:
                drawingView.drawTool = ACEDrawingToolTypePen
            case .line:
                drawingView.drawTool = ACEDrawingToolTypeLine
            case .arrow:
                drawingView.drawTool = ACEDrawingToolTypeArrow
            case .rect:
                drawingView.drawTool = ACEDrawingToolTypeRectagleStroke
            case .rectFill:
                drawingView.drawTool = ACEDrawingToolTypeRectagleFill
            case .ellipse:
                drawingView.drawTool = ACEDrawingToolTypeEllipseStroke
            case .ellipseFill:
                drawingView.drawTool = ACEDrawingToolTypeEllipseFill
            case .text:
                drawingView.drawTool = ACEDrawingToolTypeDraggableText
            case .eraser:
                drawingView.drawTool = ACEDrawingToolTypeEraser
            }
        case .background:
            if item == backgroundImageSettings.change {
                openImagePicker()
            } else if item == backgroundImageSettings.remove {
                bgImage.image = nil
            }
        }
    }
}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let aspectRatio = pickedImage.size.width / pickedImage.size.height
            let requiredHeight = self.view.bounds.size.width / aspectRatio
            
            drawingViewTopConstraint.constant = view.bounds.size.height / 2 - requiredHeight / 2
            drawingViewBottomConstraint.constant = view.bounds.size.height / 2 - requiredHeight / 2
            view.layoutIfNeeded()
            self.imagePicked = pickedImage
            
            bgImage.image = pickedImage
            drawingView.clear()
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension UIInterfaceOrientation {
    /// Gets the necessary angle to rotate by in order to compensate for device orientation.
    var angleDegrees: CGFloat {
        switch self {
        case .landscapeLeft: return 90
        case .landscapeRight: return 270
        case .portraitUpsideDown: return 180
        default: return 0
        }
    }
}

extension ViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            self.drawingView.setDraggableTextValue(textView.text)
            textView.resignFirstResponder()
            textView.text = ""
            textView.isHidden = true
            return false
        }
        return true
    }
}

